﻿using UnityEngine;
using System.Collections;

public class NumberWizard : MonoBehaviour 
{
	int max;
	int min;
	int guess;

	// Use this for initialization
	void Start () 
	{
		StartGame();
	}

	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.UpArrow)) 
		{
			//print ("The up arrow was pressed");
			min = guess;
			NextGuess();
		}

		else if (Input.GetKeyDown (KeyCode.DownArrow)) 
		{
			//print ("The down arrow was pressed");
			max = guess;
			NextGuess();
		}

		else if (Input.GetKeyDown (KeyCode.Return))
		{
			print ("The Return Key was pressed, which means I win!");
			StartGame();
		}
	}

	void StartGame()
	{	
		max = 1000;
		min = 1;
		guess = 500;

		print ("<=========================>");
		print ("Welcome to Number Wizard!");
		print ("Please pick a number in your head, but don't tell me :) ");
		
		print ("The highest number you are allowed to pick is: " + max);
		print ("The lowest number you are allowed to pick is: " + min);
		
		print ("Is the number higher or lower than " + max / 2);
		print ("Up arrow = higher, Down arrow = lower, Return means its equal");

		max = max + min;
	}

	void NextGuess ()
	{
		guess = (max + min) / 2;
		print ("Is the number Higher or Lower than " + guess);
		print ("Up arrow = higher, Down arrow = lower, Return means its equal");
	}
}
