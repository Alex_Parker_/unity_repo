﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextController : MonoBehaviour 
{
	public Text text;
	private enum States {start_page, start_introduction, start_cell, cell_death, cell_survive, cell_2_death, cell_2_survive, left, right, scene_3_death_1, scene_3_survive, scene_3_death_2}
	private States myState;

	// Use this for initialization
	void Start () 
	{
		myState = States.start_page;
	}
	
	// Update is called once per frame
	void Update () 
	{
		print (myState);
		if (myState == States.start_page) {
			start_page ();
		} else if (myState == States.start_introduction) {
			start_introduction ();
		} else if (myState == States.start_cell) {
			start_cell ();
		} else if (myState == States.cell_survive) {
			cell_survive ();
		} else if (myState == States.cell_death) {
			cell_death ();
		} else if (myState == States.cell_2_survive) {
			cell_2_survive ();
		} else if (myState == States.cell_2_death) {
			cell_2_death ();
		} else if (myState == States.left) {
			left ();
		
		//} else if (myState == States.right) {
		//	right ();
		} else if (myState == States.scene_3_death_1) {
			scene_3_death_1 ();
		} else if (myState == States.scene_3_survive) {
			scene_3_survive ();
		} else if (myState == States.scene_3_death_2) {
			scene_3_death_2 ();
		}
	}

	void start_page()
	{
		text.text = "Welcome to an original story by Alexp10v2\n\n" + "The Story of Love, Life and Death.\n\n\n\n\n\n\n\n" + "Please press \"Return\" to continue.";
		if (Input.GetKeyDown (KeyCode.Return)) 
		{
			myState = States.start_introduction;
		}
	}

	void start_introduction()
	{
		text.text = "Welcome to The Story of Life, Love and Death. A Unity-based text game created with the sole intention of painting a graphic novel in your mind, based on the choices you make.\n\n " + 
					"Through out the game, you will end up making multiple choices, each one effecting your options in future paths.\n\n" + "There are no correct paths for you to chose - your imaginiation is the limit to this game.\n\n" + 
					"Press \"Space\" to start!";

					if (Input.GetKeyDown (KeyCode.Space)) 
					{
						myState = States.start_cell;
					}
	}

	void start_cell()
	{
		text.text = "You wake up hanging from the ceiling, hanging by your ankles.\n\n" + 
					"Option 1: You observe your surroundings\n\n" + 
					"Option 2: You wonder what the f**k you did in a past life to get in this situation\n\n\n\n" + "Press \"1\" for option 1, or Press \"2\" for option 2";	

		if (Input.GetKeyDown (KeyCode.Alpha1) || Input.GetKeyDown (KeyCode.Keypad1)){
		myState = States.cell_survive;
		}
		else if (Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.Keypad2)){
		myState = States.cell_death;
		}
	}

	void cell_survive()
	{
		text.text = "While observing your surroundings, you notice the chains that are suspending you from the ceiling have some loose links due to years of neglect and rust. " +
					"You think to yourself that with some \"Persuasion\", the chain will break.\n At this point, the guard asks you to stop. " +
					"You comply and decided to answer the guard’s questions - in an effort to distract him.\n While talking, you make subtle movements to try and break the chain.\n\n " +
					"You succeed -  as you fall, the guard moves to intercept. You decided to fall with your elbow aimed for the guard.\n The guard trips and lands on his back below you as your elbow continues to fall towards his throat.\n" +
					"You killed the guard.\n\n\n\n You now have a choice:\n\n" + 
					"Option 1: Stay here and be caught and possibly punished for your actions\n\n" + 
					"Option 2: Loot the items on the guard and escape your prison.\n\n\n\n" + "Press \"1\" for option 1, or Press \"2\" for option 2";	
		
		if (Input.GetKeyDown (KeyCode.Alpha1) || Input.GetKeyDown (KeyCode.Keypad1)){
			myState = States.cell_2_death;
		}
		else if (Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.Keypad2)){
			myState = States.cell_2_survive;
		}
	}

	void cell_death()
	{
		text.text = "You try to struggle to get free, making a ruckus as you attempt to do so.\n " +
					"The guard asks you to stop and comply with his questions. You “Politely” decline to co-operate.\n" +
					"As a result, the guard starts to assault you - while you are hanging from the ceiling - practically turning you into a kick-bag.\n" +
					"You start to drift in and out of consciousness - eventually succumbing to internal bleeding on the brain.\n\n You died to the guard.\n\n\n\n" + 
					"GAME OVER - PRESS \"RETURN\" TO GO BACK TO START";
		if (Input.GetKeyDown (KeyCode.Return)){
			myState = States.start_page;
		}
	}

	void cell_2_survive()
	{
		text.text = "You realise you made quite the noise taking down the guard and think more maybe on the way - so you grab the guard’s baton, his torch and single key to the cell.\n" +
					"You use the key on the cell door - it unlocks, but the key snaps in the lock.\n You start to make your escape to freedom, but you then realise the corridor has two directions.\n " +
					"Which do you choose?\n\n" +
					"Option 1: The Left\n\n" + 
					"Option 2: The Right.\n\n\n\n" + "Press \"1\" for option 1, or Press \"2\" for option 2";
		if (Input.GetKeyDown (KeyCode.Alpha1) || Input.GetKeyDown (KeyCode.Keypad1)){
			myState = States.left;
		}
		else if (Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.Keypad2)){
			myState = States.right;
		}
	}

	void cell_2_death()
	{
		text.text = "More guards are alerted to the commotion, and storm the cell, to find you with a dead guard.\n" +
					"The instantly pin you to the ground, each taking their turn kicking your face in.\n" +
					"You eventually drift out of consciousness due to your injuries, never to regain your consciousness.\n" +
					"The guards killed you.\n\n\n\n" +
					"GAME OVER - PRESS \"RETURN\" TO GO BACK TO START";
		if (Input.GetKeyDown (KeyCode.Return)){
			myState = States.start_page;
		}
	}
	 	
	void left()
		{
		text.text = "You decide to walk left along the corridor. While walking along the corridor, you walk past some boarded up windows.\n" +
					"But you notice there is a slight crack in one of the boards. You have a look through the crack.\n" +
					"From this you gather the information that it is\n\n:" + 
					"A: Night time.\n" +
					"B: There is a strong wind and the rain is coming towards the building you are in.\n" + 
					"C: There is hardly anything out there except for what looks like your car.\n\n" +

					"After gathering this information, you begin to explore the place you are in. You come to a set of stairs.\n" +
					"As you start to climb the stairs, you notice it’s quiet, too quiet. As you take your next step, the stairs let off a massive creek due to the pressure.\n" +
					"You here movement coming from behind you.\n" +
					"What do you do?\n\n\n\n" +

					"Option 1: Continue ascending the stairs\n" +
					"Option 2: Go back and try to fend for yourself\n" +
					"Option 3: F**king leg it - making as much noise as what you imagine WWIII would sound like\n\n" + "Press \"1\" for option 1, or Press \"2\" for option 2 or Press \"3\" for option 3";

		if (Input.GetKeyDown (KeyCode.Alpha1) || Input.GetKeyDown (KeyCode.Keypad1)) {
			myState = States.scene_3_death_1;
		} else if (Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.Keypad2)) {
			myState = States.scene_3_survive;
		} else if (Input.GetKeyDown (KeyCode.Alpha3) || Input.GetKeyDown (KeyCode.Keypad3)) {
			myState = States.scene_3_death_2;
		}
	}

	void scene_3_death_1()
	{
		text.text = "You decided to continue ascending the stairs and hide around the corner at the top. The mass of voices continues to approach the stairs. You take a peak.\n" +
					"The guards notice there is nothing there and go back to playing cards. You take a sigh of relief.\n\n" +
					"As you lean back, you feel cloth behind your head. As you turn your head - your throat gets slit and you die due to blood loss.\n " +
					"While the guards were inspecting the stairs, another guard who was on the same level as you went to inspect, saw you and waited.\n" +
					"As you turned around and noticed him, he slit your throat.\n\n " +
					"As your eyes roll back into your skull, you see the glint of a cheeky smile on the guards faces, as if he is enjoying what he just did." + "The guards killed you.\n\n\n\n" +
					"GAME OVER - PRESS \"RETURN\" TO GO BACK TO START";
		if (Input.GetKeyDown (KeyCode.Return)){
			myState = States.start_page;
		}
	}

	void scene_3_death_2()
	{
		text.text = "You decide to leg it up the stairs with no disregard for the amount of noise you are creating. The guards rush to intercept you.\n" +
					"As you approach the top of the stairs a guard is waiting for you. You turn around to go back down, but there are multiple guards waiting for you at the bottom of the stairs.\n" +
					"You are cornered. The guard at the top of the stairs moves aside. You see your escape and make a break for it. That is when you hear a single gunshot.\n\n " +
					"You look down and notice a hole the size of bullet in the front of you. You were shot fatally in the back.\n " +
					"You fall to your knees. The guard from the top of the stairs approaches you, draws his knife and draws it slowly across the front of your neck.\n\n " +
					"You die due to blood loss. Your corpse falls backwards and slides down the stairs - where one of the guard’s dogs begins to eat your fingers.\n\n\n\n" +
					"GAME OVER - PRESS \"RETURN\" TO GO BACK TO START";
		if (Input.GetKeyDown (KeyCode.Return)){
			myState = States.start_page;
		}
	}

	void scene_3_survive()
	{
		text.text = "You decided to try and fend for yourself - so you head back down the stairs. You draw your baton and as you expect a mass of guards - only a single, lone guard approaches. " +
					"You hide in an adjacent door way. As the guard moves to inspect the stairs, you reveal yourself and hit your baton across the back of the guard's neck - snapping his vertebrae, but he survives. " +
					"You failed to kill him the first time. Before he screams for help, you twist his head sharply and end his suffering. To add insult to injury, you loot the guard's corpse and take his gun. " +
					"Inspecting the gun, you notice it is a Rhino .50 revolver with 4 of the 5 bullets unfired. You turn around, and begin to climb the stairs again, this time avoiding the creaky step." + 

					"You reach the top of the stairs. You are now in the annex. Ahead of you, you see an open window - big enough for a person to fit through. " +
					"You investigate the drop, and it is a sheer 60ft drop - you realise even attempting this would possibly be suicide, so you decide to continue looking around." + 
					"In doing so, you see a guard sleeping on a chair. You go to investigate but you stand on some broken glass. The sound of the glass cracking makes the guard wake up suddenly. " +
					"You practically shit yourself and start to panic what to do next. It’s at this point the guard falls back asleep as if he didn’t hear anything. " +
					"You notice the guard has a lovely serrated knife sheathed around his ankle. " +
					"What do you do?";
	}
}
